set status-left-length 40
set status-left '#[bg=#aa3334,fg=black]Rb: #S '
set status-style 'bg=#e02b2b,fg=#282d42'
set status-right "#[fg=colour182]#H #[fg=colour75]%m-%d %H:%M"
setw -g window-status-current-style "fg=red,bg=#ffdddd"
