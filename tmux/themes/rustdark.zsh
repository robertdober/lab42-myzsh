setw -g window-status-current-style "fg=blue,bg=white"
set status-left-length 40
set status-left-style 'bg=#ff9200,fg=#000000'
set status-left 'Rs: #S '
set -ag status-style 'bg=#aa6600'
set status-right "#[fg=#1a0f00]#H #[fg=#331f00]%m-%d %H:%M"
