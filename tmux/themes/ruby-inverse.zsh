set status-left-length 40
set status-left '#[bg=#222222,fg=#aaaaaa]Rb: #S '
set status-style 'bg=#444444,fg=#dd8888'
set status-right "#[fg=colour182]#H #[fg=colour75]%m-%d %H:%M"
setw -g window-status-current-style "fg=red,bg=#ffdddd"
