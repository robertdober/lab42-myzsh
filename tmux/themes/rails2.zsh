set status-left-length 40
set status-left '#[bg=#880000,fg=#cc8888]🚂: #S '
set status-style 'bg=#ccdddd,fg=#aa8888'
set status-right "#[fg=blue]#H #[fg=black]%m-%d %H:%M"
setw -g window-status-current-style "fg=red,bg=#ffdddd"

