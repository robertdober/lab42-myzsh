set status-left-length 40
set status-left '#[bg=683d80,fg=red]Ex: #S '
set status-style 'bg=#7030af,fg=black' 
set status-right "#[fg=colour182]#H #[fg=colour75]%m-%d %H:%M"
setw -g window-status-current-style "fg=blue,bg=white"
