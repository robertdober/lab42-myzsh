set status-left-length 40
set status-left '#[bg=#4d4dff,fg=blue]Ex: #S '
set status-style 'bg=#000099,fg=black'
set status-right "#[fg=colour182]#H #[fg=colour75]%m-%d %H:%M"
setw -g window-status-current-style "fg=blue,bg=white"
