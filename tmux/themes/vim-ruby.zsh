set status-left-length 40
set status-left '#[bg=#660000,fg=#f07316]VimRb: #S '
set status-style 'bg=#ffc961,fg=black'
set status-right "#[fg=#6168d0]#H %Y-%m-%d %H:%M"
setw -g window-status-current-style "fg=blue,bg=white"
