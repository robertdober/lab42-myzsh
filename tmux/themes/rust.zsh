set status-left-length 40
set status-left '#[bg=#cc7a00,fg=blue]Rs: #S '
set status-style 'bg=#ff9900,fg=black'
set status-right "#[fg=#1a0f00]#H #[fg=#331f00]%m-%d %H:%M"
setw -g window-status-current-style "fg=blue,bg=white"
