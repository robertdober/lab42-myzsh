set status-left-length 40
set status-left 'Rs: #S '
set status-left-style 'bg=#da9650,fg=black'
set status-style 'bg=#da9650,fg=blue'
set status-right "#[fg=#1a0f00]#H #[fg=#331f00]%m-%d %H:%M"
setw -g window-status-current-style "fg=#442200,bg=#dd9922"
