set status-left-length 40
set status-left '#[bg=#552244,fg=green]Go: #S '
set status-style 'bg=#66aa66,fg=black'
set status-right "#[fg=black]#H #[fg=blue]%m-%d %H:%M
setw -g window-status-current-style "fg=#880088,bg=white" 
