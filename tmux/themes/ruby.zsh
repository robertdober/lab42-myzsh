set status-left-length 40
set status-left '#[bg=#883333,fg=#aa7777]Rb: #S '
set status-style 'bg=#bb1122,fg=#44eedd'
set status-right "#[fg=colour182]#H #[fg=colour75]%m-%d %H:%M"
setw -g window-status-current-style "fg=red,bg=#ffdddd"
