set status-left-length 40
set status-left '#[bg=#4444ff,fg=#dddddd]Ex: #S '
set status-style 'bg=#222288,fg=white'
set status-right "#[fg=#ff3300]#H #[fg=colour75]%m-%d %H:%M"
setw -g window-status-current-style "fg=blue,bg=white"
