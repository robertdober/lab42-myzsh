set status-left-length 40
set status-left '#[bg=#763030,fg=white]Sc: #S '
set status-style 'bg=#702823,fg=#90d8dd'
set status-right "#[fg=#601813, bg=#a38184] #H %Y-%m-%dT%H:%M "
setw -g window-status-current-style "fg=blue,bg=white"
