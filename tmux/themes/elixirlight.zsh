set status-left-length 40
set status-left '#[bg=#b3d9ff,fg=#000099]Ex: #S '
set status-style 'bg=#b3d9ff,fg=black'
set status-right "#[fg=#ff3300]#H #[fg=colour75]%m-%d %H:%M"
setw -g window-status-current-style "fg=blue,bg=white"
