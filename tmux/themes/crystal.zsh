# Crystal Tmux Theme
set status-left-length 40
set status-left '#[bg=#21184d,fg=#9c9c9c]Crystal: #S '
set status-style 'bg=#9c9c9c,fg=black'
set status-right "#[fg=black]#H #[fg=blue]%m-%d %H:%M"
setw -g window-status-current-style "fg=#880088,bg=white" 
