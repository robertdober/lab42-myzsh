set status-left-length 40
set status-left '#[bg=#e6ffff,fg=green]Go: #S '
set status-style 'bg=#00add8,fg=black'
set status-right "#[fg=#003333]#H #[fg=#001a1a]%m-%d %H:%M"
setw -g window-status-current-style "fg=blue,bg=white"
