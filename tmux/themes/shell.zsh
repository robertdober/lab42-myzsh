set status-left-length 40
set status-left '#[bg=#9f2e84,fg=#f07316]Sh: #S '
set status-style 'bg=#d061b6,fg=black'
set status-right "#[fg=#6168d0]#H %Y-%m-%d %H:%M"
setw -g window-status-current-style "fg=blue,bg=white"
