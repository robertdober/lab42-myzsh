set status-left-length 40
set status-left '#[bg=#1f7919,fg=#f07316]Vim: #S '
set status-style 'bg=#d0c961,fg=black'
set status-right "#[fg=#6168d0]#H %Y-%m-%d %H:%M"
setw -g window-status-current-style "fg=blue,bg=white"
