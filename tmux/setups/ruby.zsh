# vim: ft=sh expandtab sw=4 sts=4 :
unalias rg 2>/dev/null

#
# Bundler
#
function b
{
    (
        source $ZshSubenvironments/bundler.zsh
        if test -z "$1"
        then
            h
        else
            eval "$@"
        fi
    )
}
