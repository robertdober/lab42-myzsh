# vim: ft=sh expandtab sw=4 sts=4 :
unalias rg 2>/dev/null

#
# Bundler
#
function b
{
    (
        source $ZshSubenvironments/bundler.zsh
        if test -z "$1"
        then
            h
        else
            eval "$@"
        fi
    )
}
# Prefixing with bundler is unneccessary for rails commands
function r
{
    (
        source $ZshSubenvironments/rails.zsh
        if test -z "$1"
        then
            h
        else
            eval "$@"
        fi
    )
}

function rk
{
    (
        source $ZshSubenvironments/rk.zsh
        if test -z "$1"
        then
            h
        else
            eval "$@"
        fi
    )
}

# function sp
# {
#     (
#         source $ZshSubenvironments/spring.zsh
#         if test -z "$1"
#         then
#             h
#         else
#             eval "$@"
#         fi
#     )
# }

#
# Cucumber
#
alias cucu="bundle exec cucumber "

#
# In case we are running MongoDB
#
# export MONGO_HOME=${HOME}/SW/databases/mongodb-linux-i686-1.6.3
# export MONGO_BIN=${MONGO_HOME}/bin
# export MONGO_DB_EXECUTABLE=${MONGO_BIN}/mongod


