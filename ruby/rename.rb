
class Object
  def fn *args
    -> (*inner_args) {
      method(args.first).(*(args.drop(1)+inner_args))
    }
  end
end
def sorted_by_date
  @__sorted_by_date__ ||= $stdin.readlines.map(&:chomp).map(&File.fn(:new)).sort_by(&:mtime)
end

def renamings
  sorted_by_date.zip(0...sorted_by_date.length).map{ |orig, prefix| [orig, ("%0#{Math.log10(sorted_by_date.length).to_i.succ}d_%s" % [prefix, orig.path])] }
end


def rename
  renamings.each do |orig, new|
    new = File.join(File.dirname(orig.path), new)
    p "#{orig.path}@#{orig.mtime} rename #{new}"
    File.rename orig.path, new
  end
end

rename
