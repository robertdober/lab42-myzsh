#!/usr/bin/env ruby
require 'securerandom'

module Main extend self
  KEY_RGX = %r{:\z}

  def main args
    parse args
    output
  end


  private
  def add_positional args
    value, *rest = args
    positionals << value
    rest
  end

  def assign_pair args
    key, value, *rest = args
    pairs.update(key.sub(KEY_RGX, "") => value)
    rest
  end

  def parse args
    loop do
      case args.first
      when nil
        break
      when KEY_RGX
        args = assign_pair args
      else
        args = add_positional args
      end
    end
  end

  def output
    with_tmpfile do |fh|
      output_pairs fh
      output_positionals fh
      puts fh.path
    end
  end

  def output_pairs fh
    pairs.each(&output_pair(fh))
  end

  def output_pair fh
    -> key, value do
      fh.puts "#{key}=#{value.inspect}"
    end
  end

  def output_positionals fh
    fh.puts "__positionals__='#{positionals.map(&:inspect).join(" ")}'"
  end

  def with_tmpfile ext=nil, &blk
    File.open(File.join(tmpdir, [SecureRandom.hex, ext].compact.join(".")), "w", &blk)
  end

  # Memoized
  # --------
  def pairs
     @__pairs__ ||= {}
  end

  def positionals
     @__positionals__ ||= []
  end

  def tmpdir
    @__tmpdir__ ||= ENV.fetch("TMP", File.join(ENV["HOME"], "tmp"))
  end

end

Main.main ARGV
