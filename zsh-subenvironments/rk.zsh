alias doc="bundle exec rake doc:app"
alias n="bundle exec rake notes:fixme"
alias n="bundle exec rake notes"
alias r="bundle exec rake routes"
alias rl='bundle exec rake routes | $PAGER'
alias s="bundle exec rake spec"
alias st="bundle exec rake stats"
alias t="bundle exec rake test"
alias .="bundle exec rake"
alias T="bundle exec rake -T"
unalias l 2>/dev/null
function l
{
    bundle exec rake -T $@
}

alias d:c='bundle exec rake db:create'
alias d:d='bundle exec rake db:drop'
alias d:m='bundle exec rake db:migrate'
alias d:s='bundle exec rake db:seed'

unalias d 2>/dev/null
function d
{
    (
        source $ZshSubenvironments/rk-d.zsh
        if test -z "$1"
        then
            h
        elif test "$1" = ":"
        then
            shift
            bundle exec rake db:${@}
        else
            eval "$@"
        fi
    )
}
function h
{
    cat <<EOS
rk enables the following subcommands, only available inside the rk command.

  d   ... subcommand for database tasks DEPRECATED
  d:c ... db:create
  d:d ... db:drop
  d:m ... db:migrate
  d:s ... db:seed

  doc .. doc:app
  f  ... notes:fixme
  l  ... list all tasks with -T
  n  ... notes
  r  ... show routes
  rl ... show routes but pipe into $PAGER
  s  ... rspec
  st ... stats
  t  ... test

  T  ... -T
  .  ... aliases back to bundle exec rake
  
  h ... shows this text
EOS
}
