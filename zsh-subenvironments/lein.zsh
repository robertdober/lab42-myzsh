alias d='lein deps'
alias n='lein new'
alias r='lein repl'
alias s='lein search'
alias t='lein test'
alias .="lein"
function h
{
    cat <<EOS
l enables the following subcommands, only available inside the l command.

  d ...  lein deps
  n ...  lein new
  r ...  lein repl
  s ...  lein search
  t ...  lein test

  . ...  lein, thus 'l .' can be used instead of 'lein'

  h ... displays this
EOS
}
