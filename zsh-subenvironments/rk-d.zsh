alias c='bundle exec rake db:test:prepare'
alias d='bundle exec rake db:schema:dump'
alias l='bundle exec rake db:schema:load'
alias lst='bundle exec rake -T db'
alias m='bundle exec rake db:migrate'
alias M='bundle exec rake db:migrate && bundle exec rake db:test:prepare && bundle exec rake db:schema:dump'
alias r='bundle exec rake db:rollback'
alias s='bundle exec rake db:seed'
alias t='RAILS_ENV=test bundle exec rake db:migrate'
alias v='bundle exec rake db:version'
unalias : 2>/dev/null
function :
{
    bundle exec rake db:${@}
}
function h
{
    cat <<EOS
d enables the following subcommands, only available inside the d command.

  c ... creates database
  d ... dumps the database into db/schema.rb
  l ... shows all database tasks
  m ... migrates the database
  r ... rolls the database back (STEP=1)
  s ... seeds the database
  t ... migrates test database
  v ... shows migration version
  : ... function to execute: bundle exec rails db:\$*
  
  h ... shows this text
EOS
}
