alias gc='mix phx.gen.context '
alias gs='mix phx.gen.secret '
alias r='mix phx.routes '
alias s='mix phx.server '
alias si='iex -S mix phx.server '
alias sct='mix phx.gen.secret '
alias seed='mix run priv/repo/seeds.exs '
alias .='mix phx '

function h
{
    cat <<EOS
m p enables the following subcommands, only available inside the m p command.

    g   ... implements the mix phx.gen subenvironment
    r   ... mix phx.routes
    s   ... mix phx.server
    si  ... iex -S mix phx.server
    sct ... mix phx.gen.secret <size>
    seed .. mix run priv/repo/seeds.exs
    .   ... routes back to mix.phx

    h   ... shows this text
EOS
}
unalias g 2>/dev/null
function g
{
    if test $# -eq 0; then
        echo "m p g needs a param"
        echo 'type `m p g h` for help'
    else
        (
        source $ZshSubenvironments/mix-phx-gen.zsh
        eval "$@"
        )
    fi
}
