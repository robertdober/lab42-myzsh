alias d='mix coveralls.detail'
alias ht='mix coveralls.html'
function h
{
    cat <<EOS
m c enables the following subcommands, only available inside the m c command.

    d   ... mix coveralls.detail
    ht  ... mix coveralls.html

    h   ... shows this text
EOS
}
