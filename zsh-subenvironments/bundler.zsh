alias e='bundle exec'
alias rails='bundle exec rails '
alias rs='bundle exec rspec '
alias sk='bundle exec sidekiq '
alias sq='bundle exec sidekiq '
alias c='bundle exec cucumber'
alias cp='bundle exec rubocop'
alias cf='bundle exec cucumber -t @focus'
alias cr='bundle exec cucumber -t @regression'
alias cw='bundle exec cucumber -t @wip'
alias q='bundle exec qed '
alias rb='bundle exec ruby '
alias rcp='bundle exec rubocop -a'
alias s='bundle show'
alias rk.='bundle exec rake'
alias .='bundle'

function h
{
    cat <<EOS
    b enables the following subcommands, only available inside the b command.

    c   ... bundle exec cucumber
    cp  ... bundle exec rubocop

    e   ... bundle exec
    q   ... bunlde exec qed
    r * ... implements the rails subcommand

    rails ... bundle exec rails
    rs    ... bundle exec rspec
    rb    ... bundle exec ruby
    rk *  ... implements the rake subcommand

    s   ... bundle show
    sk  ... bundle exec sidekiq
    sl  ... bundle show | $PAGER
    sp  ... bundle exec spring ...
    sq  ... bundle exec sidekiq

    t   ... bundle exec ruby -Itest (with parameter sanitazion of $1)
    tcp ... bundle exec ruby -Itest $@ \$(readclipboard)

    .   ... bundle         escapes from subcommand 

    h   ... shows this text
EOS
}

unalias sl 2>/dev/null
function sl {
    if test $# -gt 0
    then
        bundle show | grep "$1"
    else
        bundle show
    fi | $PAGER
}

unalias t 2>/dev/null
function t
{
        local file="$1"
        shift
        file="${file%%:*}"
        if test "/" == ${file[1]}; then
                bundle exec ruby -Itest ${file} $@
        else
                bundle exec ruby -Itest test/${file/test\//} $@
        fi
}

unalias sp 2>/dev/null
function sp
{
    bundle exec spring $*
}

unalias tcp 2>/dev/null
function tcp
{
        bundle exec ruby -Itest $@ $(readclipboard)
}

function r
{
    (
    source $ZshSubenvironments/rails.zsh
    if test -z "$1"
    then
        h
    else
        eval "$@"
    fi
    )
}

function j
{
    (
    source $ZshSubenvironments/jekyll.zsh
    if test -z "$1"
    then
        h
    else
        eval "$@"
    fi
    )
}

function rk
{
    (
    source $ZshSubenvironments/rk.zsh
    if test -z "$1"
    then
        h
    else
        eval "$@"
    fi
    )
}

function rs
{
    (

    # Backward compatibility and convenience command
    # use b rs h to get help on subcommands
    if test -z "$1"; then
        bundle exec rspec
    else
        source $ZshSubenvironments/rs.zsh
        eval "$@"
    fi
    )

}
