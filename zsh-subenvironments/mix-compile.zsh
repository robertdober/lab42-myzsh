alias f='mix compile --force'
function h
{
    cat <<EOS
m c enables the following subcommands, only available inside the m c command.

    f   ... mix compile --force

    h   ... shows this text
EOS
}
