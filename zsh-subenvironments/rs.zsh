alias d="bundle exec rspec --format=documentation"
alias t="bundle exec rspec --tag "
alias dt="bundle exec rspec --format=documentation --tag "
alias td="bundle exec rspec --format=documentation --tag "
alias u="bundle exec rspec --tag @unit "
alias du="bundle exec rspec --format=documentation --tag @unit "
alias ud="bundle exec rspec --format=documentation --tag @unit "

alias n="bundle exec rspec --tag @next "
alias dn="bundle exec rspec --format documentation --tag @next "
alias nd="bundle exec rspec --format documentation --tag @next "

alias .="bundle exec rspec "


function h
{
    cat <<EOS
rs enables the following subcommands, only available inside the rs command.

  d  ... rspec --format documentation
  t  ... rspec --tag # use @<tag> as first param
  dt ... rspec --format documentation --tag # use @<tag> as first param
  td ... rspec --format documentation --tag # use @<tag> as first param
  u  ... rspec --tag @unit
  du ... rspec --format documentation --tag @unit
  ud ... rspec --format documentation --tag @unit
  n  ... rspec --tag @unit
  dn ... rspec --format documentation --tag @next
  nd ... rspec --format documentation --tag @next
  . ... aliases back to bundle exec rspec - same as b rs but can have additonal parameters
  
  h ... shows this text
EOS
}
