alias pry='git grep binding\.pry '

alias .="git grep"
alias 'todo?'="git grep TODO: "
unalias def 2>/dev/null
function def {
        local name="$1"
        shift
        git grep $@ "def \s*\(.*\.\)\?${name}\b"
}
# unalias fn 2>/dev/null
# function fn {
#     local name="$1"
#     shift
#     git grep $@ "\bfn\s\s*${name}\b"
# }
unalias macro 2>/dev/null
function macro {
    local name="$1"
    shift
    git grep $@ "\bdefmacro\s\s*${name}\b"
}
# unalias pubfn 2>/dev/null
# function pubfn {
#     local name="$1"
#     shift
#     git grep $@ "\bpub\s\s*\bfn\s\s*${name}\b"
# }
unalias dp 2>/dev/null
function dp {
        local name="$1"
        shift
        git grep $@ "defp \s*\(.*\.\)\?${name}\b"
}
unalias da 2>/dev/null
function da {
        local name="$1"
        shift
        git grep $@ "defp? \s*\(.*\.\)\?${name}\b"
}

unalias word 2>/dev/null
function word {
        local rgx="\b${@[-1]}\b"
        local args=("${@[1,-2]}")
        echo $args
        if test -n "$args"; then
                git grep ${args} "$rgx"
        else
                git grep "$rgx"
        fi
}        
unalias h 2>/dev/null
function h
{
    cat <<EOS
gg enables the following subcommands, only available inside the gg command.

  def ... find a def
  macro ... find a defmacro
  word  ... find a word

  todo? ... find todos
  pry   ... find prys

  .   ... git grep
EOS
}

