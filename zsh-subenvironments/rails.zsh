echo 'setting up rails subenv'
alias c="bundle exec rails console"
alias d="bundle exec rails destroy"

alias dc="bundle exec rails db:create"
alias dd="bundle exec rails db:schema:dump"
alias dm="bundle exec rails db:migrate"
alias dr="bundle exec rails db:rollback"
alias ds="bundle exec rails db:seed"
unalias dreset 2>/dev/null
function dreset
{
    bundle exec rails db:drop
    bundle exec rails db:create
    bundle exec rails db:migrate
    bundle exec rails db:seed
}

alias g="bundle exec rails generate"

alias model="bundle exec rails generate model"
alias mig="bundle exec rails generate migration"
alias r="bundle exec rails runner"
alias s="bundle exec rails server"
alias t="bundle exec ruby -Ilib:test "
alias .="bundle exec rails"

unalias rt
function rt
{
    bundle exec rails routes > ${1:-tmp/routes.txt}
}

unalias gr 2>/dev/null
function gr
{
    local file=${2:-tmp/routes.txt}
    grep "$1" ${file}
}

unalias grg 2>/dev/null
function grg
{
    gr "GET.*$1" $2   
}

unalias grp 2>/dev/null
function grp
{
    gr "POST.*$1" $2   
}

unalias gru 2>/dev/null
function gru
{
    gr "PUT.*$1" $2   
}

unalias grt 2>/dev/null
function grt
{
    gr "PATCH.*$1" $2   
}

unalias gpg 2>/dev/null
function gpg
{
    gr "$1.*GET" $2   
}

unalias gpp 2>/dev/null
function gpp
{
    gr "$1.*POST" $2   
}

unalias gpu 2>/dev/null
function gpu
{
    gr "$1.*PUT" $2   
}

unalias gpt 2>/dev/null
function gpt
{
    gr "$1.*PATCH" $2   
}
unalias e 2>/dev/null
function e
{
  local x=$$
  local file=tmp/${x}.rb
  echo 'puts Rails.env' > ${file}
  bundler exec rails runner ${file}
  rm ${file}
}

unalias w 2>/dev/null
function w
{
    which $1
}

alias rk='bundle exec rake '
unalias nt 2>/dev/null
function nt {
    :> log/test.log
    bundle exec rake TESTOPTS="--name=$1"
}
function h
{
    cat <<EOS
r enables the following subcommands, only available inside the r command.

  Misc
  ====
  c  ... bundle exec rails console
  d  ... bundle exec rails destroy
  e  ... show current default environment
  g  ... bundle exec rails generate
  model ... bundle exec rails generate model
  mig   ... bundle exec rails generate migration
  r ... bundle exec rails runner
  s ... bundle exec rails server
  . ... aliases back to bundle exec rails

  Database
  ========
  dc ... bundle exec rails db:create
  dd ... bundle exec rails db:schema:dump
  dm ... bundle exec rails db:migrate
  dr ... bundle exec rails db:rollback
  ds ... bundle exec rails db:seed
  dreset ... drops, creates, migrates and seeds the database

  Routes
  ======
  rt  ... reloads routes file
  gr  ... grep routes file
  grg ... grep routes file GET
  grp ... grep routes file POST
  grt ... grep routes file PATCH
  gru ... grep routes file PUT
  gpg ... grep path file GET
  gpp ... grep path file POST
  gpt ... grep path file PATCH
  gpu ... grep path file PUT

  Tests
  =====
  rk ... bundle exec rake
  t  ... bundle exec ruby -Ilib:test
  nt ... bundle exec rake TESTOPTS="--name $1"

  w ... which in the subcommand context
  h ... shows this text
EOS
}

