subcommand='bundle exec jekyll'
alias b="$subcommand build "
alias n="$subcommand new "
alias s="$subcommand serve "
alias .="$subcommand "

function h {
    cat <<EOS
j enables the following subcommands, only available inside the j command.

  b  ... bundle exec jekyll build
  n  ... bundle exec jekyll new
  s ... bundle exec jekyll serve
  . ... aliases back to bundle exec jekyll (for more uncommon commands)

  h ... shows this text
EOS
}


