alias c='mix ecto.create '
alias gm='mix ecto.gen.migration '
alias m='mix ecto.migrate '
alias ms='mix ecto.migrations '
alias r='mix ecto.rollback '

function h
{
    cat <<EOS
m e enables the following subcommands, only available inside the "m e" command.

    c   ... mix ecto.create
    gm  ... mix ecto.gen.migration
    m   ... mix ecto.migrate
    ms  ... mix ecto.migrations
    r   ... mix ecto.rollback

    h   ... shows this text
EOS
}
