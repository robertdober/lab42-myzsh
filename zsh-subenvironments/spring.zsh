alias rs='bundle exec spring rake spec '
alias rsd='bin/rspec --format documentation '
alias rsdt='bin/rspec --format documentation --tag '
alias rstd='bin/rspec --format documentation --tag '
alias rst='bin/rspec --tag '
alias s='spring status '


unalias rsd 2>/dev/null
function rsd
{
    bin/rspec --format documentation ${1:-spec/}
}

function h
{
        cat <<EOS
        sp enables the following subcommands, only available inside the b command.

        s    ... aliased to spring status
        rs   ... aliased to bin/rspec
        rst  ... alaised to bin/rspec --tag
        rsd  ... aliased to bin/rspec --format documentation
        rsdt ... aliased to bin/rspec --format documentation --tag
        rstd ... aliased to bin/rspec --format documentation --tag

        h ... shows this text
EOS
}
