alias c='mix deps.compile'
alias g='mix deps.get '
alias u='mix deps.update '
function h
{
    cat <<EOS
m p enables the following subcommands, only available inside the m p command.

    c   ... mix deps.compile
    g   ... mix deps.get
    u   ... mix deps.update

    h   ... shows this text
EOS
}
