alias .='mix '
alias doc='mix docs '
alias eb='mix escript.build '
alias ei='mix escript.install '
alias i='iex -S mix '
alias hb='mix hex.build '
alias hs='mix hex.search '
alias t='mix test '
alias tt='mix test --trace '
alias x='mix xtra '
alias y='mix dialyzer '

function h
{
    cat <<EOS
m enables the following subcommands, only available inside the m command.

    c   ... mix compile (only w/o params)
    c   ... implements the mix compile subenbironement
    cv  ... mix coveralls (only w/o params)
    cv *... implements the mix coveralls subenvironement (only with params)
    d   ... mix deps (only w/o params)
    d * ... implements the mix deps subenvironment (only with params)
    doc ... mix docs
    e * ... implements the mix ecto subenvironment
    eb  ... mix escript.build
    ei  ... mix escript.install
    hb  ... mix hex.build
    hs  ... mix hex.search
    i   ... iex -S mix
    p * ... implements the mix phx subenvironement
    t   ... mix test
    tt  ... mix test --trace
    x   ... mix xtra
    y   ... mix dialyzer
    
    .   ... links back to the original mix command, thusly 'm .' has the same effect than 'mix'

    h   ... shows this text
EOS
}

unalias c 2>/dev/null
function c {
    if test $# -eq 0; then
        mix compile
    else
        (
        source $ZshSubenvironments/mix-compile.zsh
        eval "$@"
        )
    fi
}
unalias cv 2>/dev/null
function cv {
    if test $# -eq 0; then
        mix coveralls
    else
        (
        source $ZshSubenvironments/mix-coveralls.zsh
        eval "$@"
        )
    fi
}
unalias d 2>/dev/null
function d
{
    if test $# -eq 0; then
        mix deps
    else
        (
        source $ZshSubenvironments/mix-deps.zsh
        eval "$@"
        )
    fi
}

unalias e 2>/dev/null
function e
{
    (
    source $ZshSubenvironments/mix-ecto.zsh
    if test -z "$1"
    then
        h
    else
        eval "$@"
    fi
    )
}

unalias p 2>/dev/null
function p
{
    (
    source $ZshSubenvironments/mix-phx.zsh
    if test -z "$1"
    then
        h
    else
        eval "$@"
    fi
    )
}
