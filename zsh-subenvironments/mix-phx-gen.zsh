alias c='mix phx.gen.context '
alias ht='mix phx.gen.html '
alias j='mix phx.gen.json '
alias s='mix phx.gen.schema '
alias sec='mix phx.gen.secret '

function h
{
    cat <<EOS
m p enables the following subcommands, only available inside the m p command.

    c   ... mix phx.gen.context
    ht  ... mix phx.gen.html
    j   ... mix.phx.gen.json
    s   ... mix phx.gen.schema
    sec ... mix phx.gen.secret <size>

    h   ... shows this text
EOS
}

