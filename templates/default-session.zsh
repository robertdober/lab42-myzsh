#!/usr/bin/env zsh

export home_dir="$(cd "$1"; pwd)"
export session_name="$(basename ${home_dir})"

source $Lab42MyZsh/templates/default_session/types.zsh
function main {
echo ">>>main"
    default_session
}

source $Lab42MyZsh/templates/tmux-commands.zsh
