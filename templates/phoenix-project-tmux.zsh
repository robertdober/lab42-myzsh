export session_name="${session_name:-PhoenixDefault}"
export session_type=[Ph]
export mix_name=${mix_name:-$(basename ${session_name})}
export database_name=${database_name:-${mix_name}_dev}
export database_program=${database_program:-psql}

new_session

# Github themed default vim
new_vi
vi_colorscheme github


# lib vim
new_vi lib/${mix_name}
vi_colorscheme Mustang_Vim_Colorscheme_by_hcalves

# web vim
new_vi lib/${mix_name}_web
vi_colorscheme solarized
vi_set_command background=dark

# test
new_window 'mix test' 'mix test'

# vi test
new_vi test

# iex -S mix
new_window console 'iex -S mix'

# iex -S server
new_window server 'iex -S mix phoenix.server'

# chex window
new_window chex

new_window database ${database_program} ${database_name}
new_window  xomo xomodoro
