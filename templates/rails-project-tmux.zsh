source $Lab42MyZsh/templates/default_session/common_types.zsh
export session_name=${session_name:-RailsDefault}
export session_type='[RR]'
register_rvm
new_session

# vi
new_vi
vi_colorscheme github

# vi app
new_vi app
vi_colorscheme solarized
vi_set_command background=dark

# test
after_open_window='bundle exec rake test'
new_window 'test'

# vi test
new_vi test

# console
after_open_window='bundle exec rails console'
new_window console
