########################################################################
#
# Opens a new session depending on the project type
#
########################################################################
function default_session {

    if custom_session_is_defined
    then
        true
    elif guessed_phoenix_project
    then
        ( cd $home_dir; source $Lab42MyZsh/templates/phoenix-project-tmux.zsh)
    elif guessed_elixir_project
    then
        ( cd $home_dir; source $Lab42MyZsh/templates/elixir-project-tmux.zsh)
    elif guessed_go_project
    then
        ( cd $home_dir; source $Lab42MyZsh/templates/go-project-tmux.zsh )
    elif guessed_rails_rspec_project
    then
        ( cd $home_dir; source $Lab42MyZsh/templates/rails-rspec-project-tmux.zsh )
    elif guessed_rails_project
    then
        ( cd $home_dir; source $Lab42MyZsh/templates/rails-project-tmux.zsh )
    elif guessed_haskell_project
    then
        ( cd $home_dir; source $Lab42MyZsh/templates/haskell-project-tmux.zsh )
    elif guessed_ruby_rspec_project
    then
        ( cd $home_dir; home_dir=$PWD source $Lab42MyZsh/templates/ruby-rspec-project-tmux.zsh )
    elif guessed_ruby_lib_project
    then
        ( cd $home_dir; source $Lab42MyZsh/templates/ruby-project-tmux.zsh )
    elif guessed_ruby_project
    then
        ( cd $home_dir; source $Lab42MyZsh/templates/ruby-project-tmux.zsh )
    elif guessed_scala_lib_project
    then
        ( cd $home_dir; source $Lab42MyZsh/templates/scala-lib-project-tmux.zsh )
    elif guessed_rust_cargo_project
    then
        ( cd $home_dir; source $Lab42MyZsh/templates/rust-cargo-project-tmux.zsh )
    elif guessed_go_project
    then
        ( cd $home_dir; source $Lab42MyZsh/templates/go-project-tmux.zsh )
    else
        ( cd $home_dir; source $Lab42MyZsh/templates/default-project-tmux.zsh )
    fi
}


########################################################################
#
#   Guess Project Type
#
########################################################################
function guessed_elixir_project {
    test -r $home_dir/mix.exs
}

function guessed_go_project
{
    expr $PWD : "$HOME/go" >/dev/null 2>&1
}

function guessed_rust_cargo_project {
    test -r $home_dir/Cargo.toml
}

function guessed_phoenix_project {
    test -r $home_dir/mix.exs && test -d $home_dir/web
}
function guessed_rails_rspec_project {
    test -r $home_dir/Gemfile && test -d $home_dir/spec && grep 'gem .rails.' $home_dir/Gemfile >/dev/null
}

function guessed_rails_project {
    test -r $home_dir/Gemfile && grep 'gem .rails.' $home_dir/Gemfile >/dev/null
}

function guessed_haskell_project {
    test -r $home_dir/Setup.hs >/dev/null 2>&1 || test -r $home_dir/$(basename $home_dir).cabal >/dev/null 2>&1
}

function guessed_ruby_rspec_project {
    test -r $home_dir/Gemfile && grep 'gem .rspec.' $home_dir/Gemfile >/dev/null

}

function guessed_ruby_lib_project {
    ls $home_dir/*.gemspec >/dev/null 2>&1
}

function guessed_ruby_project {
    ls $home_dir/Gemfile >/dev/null 2>&1
}

function guessed_scala_lib_project {
    test -r $home_dir/build.sbt
}

########################################################################
#
# Check for custom session
#
########################################################################
function custom_session_is_defined {
set -x
    if test -r $home_dir/.tmux.zsh; then
        source $home_dir/.tmux.zsh
    else
        false
    fi
}
