export session_name=${session_name:-RailsDefault}
export session_uses_rvm=0
source $Lab42MyZsh/tools/rvm.zsh
load_correct_ruby
new_session

# vi
new_vi
vi_colorscheme github

# vi app
new_vi lib
vi_colorscheme solarized
vi_set_command background=dark

# test

if test -r .rspec; then
    new_window spec 'bundle exec rspec'
    named_vi 'vi spec' spec
elif test -d test; then
    new_window test 'bundle exec rake test'
    named_vi 'vi test' test
fi

# console
gem_name=$(ls *.gemspec | sed 's/\.gemspec//')
# lab42 namespaces
if expr "$gem_name" : 'lab42_'; then
    gem_name=${gem_name/lab42_/lab42\/}
fi
new_window console
if test -n "${gem_name}"; then
    send_keys "bundle exec pry -I./lib -r $gem_name"
else
    send_keys "bundle exec pry -I./lib"
fi



# demos?

if test -d ./demo; then
    named_vi "vi demo" "demo '-c colorscheme 256_asu1dark'" 
    new_window qed
fi
