# # Usage:
#
# ## Basic Procedure:
#
# * Define the needed env vars via export in the client script.
# * Write a main function in the client script, this function can access the functions provided
#     in this file. And must start with a new_session function call or a default_session call.
# * Source this file at the end of the client script.
#
# ## Needed Environment Variables:
#
# * $HOME (of course)
# * $home_dir all windows of the session will cdir there before any action (c.f. `go_home`)
#
# ## Optional Environment Variables:
#
# * $session_name this is the name of the tmux session; `main` will only be called if no such
#    session exists yet. But the script *always* attaches to the session. It defaults to the
#    basename of $home_dir.
# * $after_open_window, if provided the content is sent as keys to the last opened window
# * after_open_window, if defined (as a function or alias) it is executed after opening a
#    new window. It will typically contain functions defined here, or call shell scripts or load
#    environments (e.g. for rvm, nvm or python's virtualenv).

source ${Lab42MyZsh}/tools/files.zsh
export session_name=${session_name:-$(basename $home_dir)}

export window_rc_file=${window_rc_file:-.tmux.window}
export session_rc_file=${session_rc_file:-.tmux.session}


export virtualenv_home=${virtualenv_home:-$Sw/Virtualenv}

source $Lab42MyZsh/templates/default_session/types.zsh

function __after_open_session__ {
        for line in ${after_open_session}; do
                send_keys "${line}"
        done
        if type after_open_session 2>/dev/null >&2; then
                after_open_session
        fi
}
function __after_open_window {
        for line in ${after_open_window}; do
                send_keys "${line}"
        done
        if type after_open_window 2>/dev/null >&2; then
                after_open_window
        fi
}

function attach_to_session {
        tmux attach -t ${session_name}
}

unalias go_to 2>/dev/null
# change to indicated dir (use home for a more readable invocation of go_home)
function go_to {
        if test "$1" == "home"
        then
                go_home
        else
                send_keys "cd $1"
        fi

}
# `go_home` sends a cd $home_dir to the current window
function go_home {

        send_keys "cd ${home_dir}"
        # tmux send-keys -t ${session_name}:${current_window} "cd ${home_dir}" C-m
}

# opens a new session
function new_session {
        tmux source-file ${HOME}/.tmux.conf
        tmux new-session -s ${session_name} -d -n ${1:-"sh"}
        tmux set-window-option -g automatic-rename off
        # Allow for many files in the env var
        go_home
        source_files ${session_rc_file}
        __after_open_session__
        __init_window__
}

#
# `new_window` [name=sh] [*cmds]
function new_window {
        local name=${1:-sh}
        tmux new-window -t ${session_name} -n ${name}
        current_window=$((${current_window}+1))
        go_home
        __init_window__
        shift || : # in case of default
        send_keys "export tmux_session_type=$session_type"
        local lastkeys=""
        for i in $*
        do
                send_keys "$i"
                lastkeys="$i"
        done
        # register_keys "$lastkeys"
}

function new_console_window {
       new_window console
       send_keys $console_command
}

function register_keys
{
    send_keys "export _tmux_last_sent_keys='$1'"
}
function select_window
{
        tmux select-window -t :$1
}
function source_file
{
        send_keys "test -r $1 && source $1"
}

function source_files {
        for file; do
            if test -r $file
            then
                source_file ${file}
            fi
        done
}

function __init_window__ {
        tmux set-window-option -t ${session_name}:${current_window} automatic-rename off
        tmux set-window-option -t ${session_name}:${current_window} allow-rename off
        source_files ${window_rc_file}
        send_keys "export tmux_session_type=\"$session_type\""
        __after_open_window
}
# `open_vi` [dir=. [*cmds]]
# sends vi $dir to the current window and then sends each command to the window
# e.g.
#
#     open_vi $HOME/src ':colorscheme solarized' ':set background=light'
function open_vi {
        local dir=${1:-'.'}
        shift || : # in case of default
        send_keys "vi ${dir}"
        for i in $*
        do
                send_keys "$i"
        done
}

# Opens a new window with a compact vi command (allowing for CL editing)
function new_vi_with_commands
{
    new_window "vi $1"
    local arg="$1"
    shift
    local commands=""
    for command
    do
        commands="${commands} -c \"${command}\""

    done
    send_keys "vi ${arg}${commands}"
}

# `new_vi` dir *cmds
# is a shortcut for
# `new_window dir`
# `open_vi dir *cmds`
function new_vi {
        new_window $1
        open_vi $*
}
function new_vi_for {
    for dir
    do
        if test -d $dir
        then
            new_vi $dir
        fi
    done
}
function vi_colorscheme {
        send_keys ":colorscheme $1"
}
function vi_set_command {
    send_keys ":set $1"
}

function named_vi
{
    local name="$1"
    shift
    new_window $name
    send_keys "vi $@"
}

function send_keys {
        tmux send-keys -t ${session_name}:${current_window} "$@" C-m
}

function send_keys_raw {
        tmux send-keys -t ${session_name}:${current_window} "$@"
}

function send_delayed_keys {
        local delay=$1
        shift
        for keys
        do
                send_keys_raw $keys
                sleep $delay
        done
}

function vi_with_commands {
    send_keys "vi $*"
}
function activate_virtualenv
{
        local env=$1
        source_file ${virtualenv_home}/${env}/bin/activate
}
alias localTmux='zsh .local.tmux'

#

export current_window=${current_window:-0}
export after_open_window=${after_open_window:-''}

if tmux has-session -t ${session_name} 2>/dev/null >&2
then
else
    typeset -axg after_open_window
    typeset -axg after_open_session
    main
fi
attach_to_session

