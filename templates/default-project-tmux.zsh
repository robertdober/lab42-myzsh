export session_name=${session_name:DftSess}
export session_type='[Dft]'
new_session

# Github themed default vim
new_vi_with_commands "." "colorscheme github"

new_window '2nd shell'

new_vi_with_commands "." "colorscheme slate"

new_window console
