new_session

# vi
new_window vi
send_keys  "vi . -c 'set background=dark' -c 'colorscheme github'"

# vi lib
new_window "vi lib"
send_keys  "vi lib -c 'set background=dark' -c 'colorscheme solarized'"

new_window rspec

# vi test
new_window "vim spec"
send_keys "vi spec"

# console
new_window console
send_keys "bundle exec pry -I./lib"
