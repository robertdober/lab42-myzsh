export session_name=${session_name:-ElixirDefault}
export session_type='[E]'
new_session

# Github themed default vim
new_vi
vi_colorscheme github

# vi lib
new_vi lib
vi_colorscheme solarized
vi_set_command background=dark

# test
after_open_window='mix test'
new_window 'mix test'

# vi test
new_vi test

# iex -S mix
after_open_window='iex -S mix'
new_window console
