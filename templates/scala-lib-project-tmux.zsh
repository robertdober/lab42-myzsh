export session_name=${session_name:-ScalaDefault}
export session_type='[S]'
new_session

# Github themed default vim
new_vi_with_commands . "colorscheme github"

# vi src/main
new_vi_with_commands src/main/scala "colorscheme solarized" "set background=dark"

# shell
new_window

# vi src/test
new_vi_with_commands src/test/scala "colorscheme solarized"

# sbt
new_window sbt sbt

# console
# after_open_window=console
new_window "sbt console" "sbt console"
