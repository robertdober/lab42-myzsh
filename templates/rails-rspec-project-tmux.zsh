export session_name=${session_name:-RailsRspec}
export session_type='[RRR]'
new_session
typeset -a after_open_window

new_session

# vi
new_vi
vi_colorscheme github

# vi app
new_vi app
vi_colorscheme solarized
vi_set_command background=dark

# spec
after_open_window+='bundle exec rake rspec'
new_window 'spec'

# vi spec
new_window 'vim spec'
open_vi spec

# console
after_open_window+='bundle exec rails console'
new_window console

# server
after_open_window+='bundle exec rails server'
new_window server
