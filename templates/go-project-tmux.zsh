export session_name=${session_name:-ElixirDefault}
export session_type=[Go]
new_session

# Github themed default vim

new_vi_with_commands . "colorscheme github"

new_window

new_vi_with_commands . "colorscheme solarized"

new_window console
