export session_name=${session_name:-ElixirDefault}
export session_type='[RC]'
new_session

# Github themed default vim
new_window vi
vi_with_commands . '-c "colorscheme github"'

new_window 'vi src'
vi_with_commands src '-c "colorscheme solarized" -c "set background=dark"'

new_window 'test'
new_window 'vi test'
vi_with_commands test '-c "colorscheme solarized"'

new_window console
