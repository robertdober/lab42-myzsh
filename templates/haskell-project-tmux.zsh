export session_name=${session_name:-HaskellDefault}
export session_type=[H]
new_session

# Github themed default vim
 new_vi
vi_colorscheme github

new_vi_for lib src
new_vi tests
vi_colorscheme solarized

new_window

# # vi test
# new_vi test

# iex -S mix
after_open_window='ghci'
new_window console
