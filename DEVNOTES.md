# v0.1.5 2017-03-15

## Renaming Refactoring Support

- use `git grep '\\bsome_string'` or something similar, once you are happy with it we want:

  - the files containing these strings in a vim with `set hidden`

e.g.

```sh
function open_refactoring {
  vim -p $( git grep -l "$1" )
}
```
But let us call it `vigrep`


