# Lab42MyZsh

## CHANGE LOG

### v2 2020-02-10

### v0.2 Lexa

#### v0.2.0

* 2017-12-21  Readd def_named_project
* 2017-12-18  Cleanup
* 2017-10-29  tools/tmux new fns before_init & export_env_var 
* 2017-08-18  tools/tmux/
* 2017-08-01  Simplify tmux init

### v0.1 Harper

2017-07-28 v0.1.12.5 fixed file= spurious output, implement tmuxreplay window command and deprecate old tmux templates
2017-07-26 v0.1.12.4 better Ga commands
2017-07-02 v0.1.12.3 bugfixes
2017-06-01 v0.1.12.2 bugfixes and vim8 adaptations
2017-05-10 v0.1.12.1 grep routes
2017-05-10 v0.1.12  rake subcommands with ':'
2017-05-09 v0.1.11 ...

* vip extensions (`profiles/084_for_vim.zsh`)

#### v0.1.10 2017-05-05

* Ga* commands to operate git commands on an Alternative Branch

### v0.1.9.2 2017-04-10

* Error in default_session

### v0.1.9.1 2017-04-10

* Error in load_config_from_file

### v0.1.9 2017-04-10


* Glitch in `templates/tmux_commands.zsh`
* `grelease_tag`
* removal of git subenvironment

### v0.1.8 2017-04-10

* vim profiles loaded from config file

### v0.1.7 2017-04-10

* vip vim with profiles

#### v0.1.6 2017-03-23

* go project detection moved up as go projects are always in the same parent dir but contain often Gemfiles

* default session for tmux, bugfix in ruby project session

* reducing function `gg` (git grep) to an alias, for flexibility

* private (defp) and all (defp?) versions for `ggd` function

* cleanup

#### v0.1.5 2017-03-16

* renaming refactoring support → `vigrep`

#### v0.1.4 2017-03-15

* removed unused git subcommands

#### v0.1.3 2017-03-14

* merge linux → master
* remove `-a` from `gim` and `gamend` aliases

#### v0.1.2 2017-03-13

* rust-cargo-project-tmux template

#### v0.1.1 (Harper) 2017-03-06

* Merged linux -> master (using /usr/bin/env shebang)

* More git wisdom see below


#### v0.1 (Harper) 2016-11-26

* Added Scala Lib default project type

* Refact of `templates/templates/tmux-commandes.zsh`

#### v0.0.2 2016-08-10

##### Extended Default tmux project templates

* Elixir

* Haskell

* Phoenix

* Rails

* Rails-Rspec

* Ruby

### v0.0.1 2016-05-01

#### Clipboard aware functions and aliases

This works best with tools like [Lab42Vim](https://github.com/RobertDober/lab42-vim) that set
the Clipboard on certain commands e.g. `:W` which saves the filename to the clipboard before
writing a buffer, or the `:ExpFilepath` command.

* function subcommand `b tcp` to  do `bundle exec ruby -Itest $@ $(xclip -o -selection clipboard)`
* function `vimcp` to `vim $@ $(xclip -o -selection clipboard)`
...

## Git Wisdom

### Problem

* We have a branch `new`which is completely intermingled with the commit history of upstream merges from branch `dev`.
* We want to have the branches history (possibly leaned out on top of the rest).


### Solution

#### Create a delta between `work` and `new` with an upstream history
1. `git fetch upstream dev; gco FETCH_HEAD -b work # One day I will find a way to do this with one git command`
1. `git merge --squash new                         # No commits, working tree is updated but not the index/staging area ???`
1. `git reset                                      # index/staging are too. I can see delta with git diff and git stash \o/`

Remark that `git fetch ...; gco FETCH_HEAD -b ...` is so common that we implemented `gfandco`

#### Adding things back

1. Repeat adding changes and committing them until `git diff work` is empty.
1. `gco work; git reset --hard work                # --hard is not needed if both branches are clean`

#### Here U go

## Installation

* Clone the repository into $YourDir

* Add `source $YourDir/lab42-my.zsh` into your `~/.zshrc`.
