require_relative './extract_lines/extractor'

def main args, input
  ExtractLines::Extractor.new(args).run(input)
end

if $0 == __FILE__
  input = $stdin.readlines(chomp: true)
  main(ARGV, input)
end
