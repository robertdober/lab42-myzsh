require 'lab42/rgxargs'

class FindFile

  Parser = Lab42::Rgxargs.new

  attr_reader :kwds, :positionals

  def run
    _run($stdin.each_line(chomp: true).lazy)
  end

  private
  def initialize args
    @kwds, @positionals, errors = Parser.parse args 
    p kwds
    p positionals
    errors.each do |error|
      $stderr.puts "Error, illegal argument #{error}"
    end
  end

  def _run(input)
    p input.map{|l| File.new(l).stat}.to_a
  end
  
end
