class Set
  def to_proc(index: 0)
    ->(*a){
      member?(a[index])
    }
  end
end
