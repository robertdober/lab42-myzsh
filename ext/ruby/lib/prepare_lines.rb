require 'securerandom'

module PrepareLines extend self

  MAX = 2**60

  def from_hex(lines)
    lines
      .map(&method(:_from_hex))
  end

  def to_hex(lines, length=8)
    lines
      .map{ |line| _to_hex(line, length) }
  end

  def move(split_fn, new_head)
    %{mv "#{split_fn.join(".")}" "#{([new_head] + split_fn[1..1]).join(".")}"}
  end

  def prepare(lines)
    lines
      .map(&:chomp)
      .reject(&File.method(:directory?))
      .map(&splitter)
  end

  def randomize(lines, length=10)
    transform(prepare(lines), _randomize(length))
  end

  def transform(lines, fn)
    lines
      .map{ |line| move(line, fn.(line)) }
  end

  private
  def _from_hex(split)
    head, ext = split
    [head.to_i(16), ext]

  end

  def _to_hex(split, length)
    head, ext = split
    [head.to_s(16).rjust(length, "0"), ext].compact
  end

  def _randomize(length)
    -> _split_fn do
      SecureRandom.rand(MAX).to_s(16).rjust(length, "0")
    end
  end

  def splitter
    @__splitter__ ||=
      -> line do
        splitter_rgx.match(line)&.captures || [line]
      end
  end

  def splitter_rgx
    @__splitter_rgx__ ||= %r{(.*)\.([^.]*)}
  end
end
