module Lab42
  class Args


    def positionals
       @__positionals__ ||= _parse && @positionals
    end

    private

    def initialize args
      @args = args
      @positionals = []
    end


    def _extract_arg arg
      case arg
      when /\A(?:[-+]?\d+)(?:[,:](?:[-+]?\d+))*\z/
        @positionals << _make_ints(arg)
      when /\A[[:alnum:]]+\z/
        @positionals << arg
      else
        raise "not yet implemented"
      end
    end

    def _make_ints arg
      segments = arg.split(",").map(&method(:_make_range))
      Set.new(segments.flatten)
    end

    def _make_range arg
      first, last = arg.split(":")
      last ||= first
      [*first.to_i()..last.to_i]
    end

    def _parse
      @args.each(&method(:_extract_arg))
      true
    end
  end
end
