require 'prepare_lines'
describe PrepareLines do

  context ".prepare" do

    let(:lines) {[
      "alpha.beta\n", "gamma", ".delta\n"
    ]}
    let(:result) {[
      ["alpha", "beta"], ["gamma"], ["", "delta"]
    ]}


    it "chomps and splits lines" do
      expect(described_class.prepare(lines)).to eq(result)
    end

  end

  context ".prepare with directories" do

    let(:lines) { ["alpha", "beta"] }
    before do
      allow(File).to receive(:directory?).with("alpha").and_return false
      allow(File).to receive(:directory?).with("beta").and_return true
    end

    it "eliminates them" do
      expect(described_class.prepare(lines)).to eq([ ["alpha"] ])
    end
  end


  context ".mv" do
    it "creates a bash command to move a file to a different head" do
      expect(described_class.move(["alpha", "beta"], "gamma"))
        .to eq(%{mv "alpha.beta" "gamma.beta"})
    end
  end

  context "transformers" do
    describe "from_hex" do
      let(:lines) {[
        ["19a0997f7e47fde", "m4v"], ["24bcf5f9d5cbe3e7", "mp4"]
      ]}
      let(:result) { [[115415288755027934, "m4v"], [2647261134369580007, "mp4"]] }

      it "parses them into ints" do
        expect(described_class.from_hex(lines)).to eq(result)
      end
    end
  end

  describe "to_hex" do
    let(:lines) { [[10], [64, "a"]] }

    it "creates hexes with default length" do
      expect(described_class.to_hex(lines))
        .to eq([["0000000a"], ["00000040", "a"]])
    end
    it "creates hexes of given length" do
      expect(described_class.to_hex(lines, 10))
        .to eq([["000000000a"], ["0000000040", "a"]])
    end
  end

  describe ".randomize" do
    let(:lines) {%w{alpha beta.gamma}}
    let(:max) { 2**60 }

    before do
      expect(SecureRandom).to receive(:rand).with(max).and_return(10)
      expect(SecureRandom).to receive(:rand).with(max).and_return(64)
    end
    it "formats the randomized values" do
      expect(described_class.randomize(lines))
        .to eq([
          %{mv "alpha" "000000000a"},
          %{mv "beta.gamma" "0000000040.gamma"}
      ])
              
    end

  end
end

