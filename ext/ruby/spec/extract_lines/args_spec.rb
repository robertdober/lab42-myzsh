require 'args'

RSpec.describe Lab42::Args do 

  describe "simple cases" do
    it "works for the empty edge case" do
      expect(positionals([])).to eq([]) 
    end

    it "works for strings" do
      expect(positionals(%w{a b c})).to eq(%w{a b c})
    end

    it "ints are converted" do
      expect_numeric_sets(%w{1 -42},[1], [-42])
    end
  end

  describe "ranges and lists of ranges" do
    it "two simple examples" do
      expect_numeric_sets(%w{0,2,5 -1:3},[0, 2, 5], [*-1..3])
    end

    it "one complex example" do
      expect_numeric_sets(%w{1,-2:3,7:8}, [-2,-1,0,1,2,3,7,8])
    end
  end

  def positionals(args)
    described_class.new(args).positionals
  end

  def expect_numeric_sets(args, *expected)
    expect( positionals(args) ).to eq(expected.map{|e| Set.new(e)})
  end

end
