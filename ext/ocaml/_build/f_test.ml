open OUnit2
open F

let assert_double expected input = 
    assert_equal expected (double input) ~printer:string_of_int

let tests = "tests for double" >::: [
    "zero" >:: (fun _ -> assert_double 0 0);
    "pos"  >:: (fun _ -> assert_double 42 21);
    "neg"  >:: (fun _ -> assert_double (-2) (-1));
]

let _ = run_test_tt_main tests
