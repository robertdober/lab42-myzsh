#!/usr/bin/env zsh

export session_name="${session_name:-PhoenixDefault}"
export session_type=[Ph]
export mix_name=${mix_name:-$(basename ${session_name})}
export database_name=${database_name:-${mix_name}_dev}
export database_program=${database_program:-psql}
source $Lab42MyZsh/tools/tmux.zsh

function init_new_session {

    tmux send-keys "cd assets" C-m
    new_window mainvim 'nip git .^ priv/repo assets'

    new_window vilib "nip lib lib/${mix_name}"
    new_window viweb "nip web lib/${mix_name}_web"

    new_window 'mix test'

    new_window vitest "nip spec test/{.,$mix_name}"
    new_window console 'iex -S mix'
    new_window server "mix phx.server"

    new_window scratch notes
    new_window1 database ${database_program} " " ${database_name}
    new_window1 xomo xomodoro
    select_window 3

}

attach_or_create

