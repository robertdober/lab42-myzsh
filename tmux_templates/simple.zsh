#!/usr/bin/env zsh

source $Lab42MyZsh/tools/tmux.zsh

function init_new_session {

    new_window mainvim 'nip git .'

    new_window vilib "nip lib ."

    new_window 'sh'
    new_window etc
}

attach_or_create

