#!/usr/bin/env zsh

source $Lab42MyZsh/tools/tmux.zsh

function init_new_session {

    new_window mainvim 'nip git .'

    new_window visrc "nip gruv src"

    new_window1 'npm test'

    new_window vitest "nip spec test"
    new_window console 'node'
    new_window1 xomo xomodoro
    new_window scratch notes
    select_window 3
}

attach_or_create
