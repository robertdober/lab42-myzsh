#!/usr/bin/env zsh

source $Lab42MyZsh/tools/tmux.zsh

function init_new_session {

    new_window mainvim 'nip git .'

    new_window vilib "nip lib lib/{.,$session_name}"

    new_window 'mix test'

    new_window vitest "nip spec test/{.,$session_name}"
    new_window console 'iex -S mix'
    new_window etc
    new_window1 xomo xomodoro
    new_window scratch notes
    select_window 3
}

attach_or_create
