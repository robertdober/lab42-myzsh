{
    print "tmux new-window -n " $1
    for (x=2; x<NF; x++) {
        print "tmux send-keys " $x " Space"
    }
    if (NF > 1) print "tmux send-keys " $NF " C-m"
}
