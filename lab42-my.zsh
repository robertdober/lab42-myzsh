#!/usr/bin/env zsh # for filetype only, this file will be sourced

export Lab42MyZsh=$(dirname $0)

for file in $Lab42MyZsh/zsh-profiles/*.zsh
do
    source ${file}
done

for completion in  $Lab42MyZsh/zsh-completions/zsh_completion.zsh ~/etc/zsh-completions/git-flow-completion.zsh
do
    test -r ${completion} && source ${completion}
done

##
#
# Local Configurations
#
##
# for file in $Lab42MyZsh/local/*.zsh
# do
#     test -r ${file} && source ${file}
# done
