# vim: ft=zsh sw=4 :
# Unify PATH
export ZshProfiles=$Lab42MyZsh/zsh-profiles
export ZshSubenvironments=${Lab42MyZsh}/zsh-subenvironments
export ZshCompletions=${Lab42MyZsh}/zsh-completions
fpath=($ZshCompletions $fpath)

# Up and Down Arrow behavior
autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
[[ -n "${key[Up]}" ]] && bindkey "${key[Up]}" up-line-or-beginning-search
# [[ -n "${key[Down]}" ]] && bindkey "${key[Down]}" down-line-or-beginning-search
export Tmp=$HOME/tmp
export TMP=$HOME/tmp
export TMPDIR=${HOME}/tmp
export DATADIR=${HOME}/data/private
# Create the name of a unique file in the temporary directory
# absolute path



# export EDITOR=/usr/local/bin/vim
export EDITOR=nvim

unalias .. 2> /dev/null
function ..
{
    cd ..
    for dir
    do
        cd $dir
    done
}
unalias ... 2> /dev/null
function ...
{
    cd ../..
    for dir
    do
        cd $dir
    done
}
unalias .... 2> /dev/null
function ....
{
    cd ../../..
    for dir
    do
        cd $dir
    done
}
unalias ..... 2> /dev/null
function .....
{
    cd ../../../..
    for dir
    do
        cd $dir
    done
}

# export DISPLAY=${DISPLAY:-:0.0}
unalias notes 2>/dev/null
alias notes="nip gruv $DATADIR/NOTES.md $DATADIR/TODOS/"


function expand_rel_path_with
{
    if expr "$1" : "\/" >/dev/null; then
        echo "$1"
    else
        echo "$2/$1"
fi }
#
# Configuration
#
function load_config_from_file
{
    local file=$(expand_rel_path_with $1 $Lab42MyZsh)
    local var=$2
    local eles key value
    typeset -gxA $var
    local tmpfile="$TMP/_load_config_from_file.$$"
    awk '{ key=$1; $1="";gsub("\"","\\\""); gsub("^ *","\"");gsub("$","\""); print "'$var'["key"]=" $0}' $file > $tmpfile
    source $tmpfile
    rm $tmpfile
}


function _help {
    local topic="$1"
    shift
    local file=$(expand_rel_path_with $topic $Lab42MyZsh/config/help)
    echo "man page for $(colorize_green $topic)" 
    echo ""
    cat ${file}
    echo ""
    for line in $*
    do
        echo "$line"
    done
}
