if test $(uname) != "Linux"
then
    return
fi
alias pbcopy='xsel --clipboard --input'
alias pbpaste='xsel --clipboard --output'
export Lab42BrewBin=/home/linuxbrew/.linuxbrew/bin/
PATH=/home/linuxbrew/.linuxbrew/sbin:$Lab42BrewBin:$PATH:/snap/bin
function _MkCamelCase {
    echo "$1" | sed -r 's/(^|_|-)([a-zA-Z0-9])/\U\2/g'
}
# Project Homes
# =============
#   Base Dirs
#   ---------
export GitHome=$HOME/gh
export ElixirGitProjects=$GitHome/elixir
for ep in $ElixirGitProjects/*; do
    test -d $ep || continue
    local name=ProjectHome$(_MkCamelCase $(basename $ep))
    eval "export $name=$ep"
done
export ProjectEarmark=$ElixirGitProjects/earmark
# Extract project locations from project files into env vars
# for project_definition in $Lab42ProjectDir/*
# do
#     dir=$(sed -ne 's/.*export tmux_session_home_dir="\(.*\)"/\1/p' ${project_definition})
#     var="Project$(basename ${project_definition})"
#     eval "export ${var}=${dir}" 2>/dev/null
# done
export Lab42ProjectTypes=$Lab42MyZsh/project_types

unalias readclipboard 2>/dev/null
# readclipboard [selection:=clipboard]
function readclipboard
{
    local selection=${1:-clipboard}
    xclip -o -selection $selection
}
unalias writeclipboard 2>/dev/null
# writeclipboard text [selection:=clipboard]
function writeclipboard
{
    local selection=${2:-clipboard}
    echo "$1" | xclip -i -selection $selection
}

echo
