local_file=$0

unalias gg 2>/dev/null
function gg 
{
    (
        source $ZshSubenvironments/git-grep.zsh
        if test -z "$1"
        then
            h
        else
            eval "$@"
        fi
    )
}

alias g='git grep '
alias gst='git status '
alias ga='git add '


alias gca='git commit --amend --date="$(date -R)" '
#
#
#   git related aliases and functions
#
alias gckws='git diff --check ' # checks for trailing whitespace
# alias gim="git commit -vm "
# alias gima="git commit -avm "
# changing last snapshot
alias gamend="git commit --amend -vm "

alias gpfwl="git push --force-with-lease"
alias gpuo="git push -u origin "
alias gsize="git count-objects -vH"

unalias gpnew 2>/dev/null
function gpnew {
    git push --set-upstream origin "$(current_branch)"
}
# unalias ngc 2>/dev/null
# function ngc { EDITOR=nvim gc $@ }
unalias ggc 2>/dev/null
function ggc { git grep "class \s*$@\b" }
unalias ggm 2>/dev/null
function ggm { git grep "module \s*$@\b" }
unalias applyex 2>/dev/null
unalias gsed 2>/dev/null
function gsed {
    git grep -l "$1" |\
    while read file_to_change; do
        ex -u /dev/null -c "%s/$1/$2/g|wq" ${file_to_change}
    done
}
unalias gwsed 2>/dev/null
function gwsed {
    git ls-files | \
    xargs ex -u /dev/null -c "%s/\\<$1\\>/$2/g|wq"
}
unalias ggn 2>/dev/null
alias ggn='git grep -n'
unalias ggf 2>/dev/null
alias ggf='git grep -l'


unalias gtake-ours 2>/dev/null
function gtake-ours {
    git checkout --ours $@
    git add $@
}
unalias gtake-theirs 2>/dev/null
function gtake-theirs {
    git checkout --theirs $@
    git add $@
}

unalias gs 2>/dev/null
alias gs='git show'
# alias gdc='git diff --cached'
# alias gdck='git diff --check'
# alias gds='git diff --stat'

unalias ggall 2>/dev/null
alias ggall='git rev-list --all | xargs git grep '
unalias grs 2>/dev/null
alias grs='git remote show '
unalias grsu 2>/dev/null
alias grsu='git remote show upstream'

unalias grso 2>/dev/null
function grso
{
    if test -z "$1"
    then
        git remote show origin
    else
        git remote show origin | grep "$1"
    fi
}
unalias gsq 2>/dev/null
# I will reset to HEAD~$1 add all files again and commit with $2
# so I can remove temporary commit messages
function gsq
{
    local n=${1}
    local message="${2}"
    if test -n "${message}"
    then
        git reset HEAD~$n
        git add .
        git commit -m "${message}"
    else
        echo "usage: sq n message\n   n number of commits\n   message new commit message" >&2
    fi

}

alias gca='git commit --amend --date="$(date -R)" '
unalias galast 2>/dev/null
alias galast='git commit -a --amend --no-edit --date="$(date -R)" '

unalias gbsetupstream 2>/dev/null
function gbsetupstream
{
    local branch=$1
    local remote=${2:-origin}
    git branch --set-upstream-to=${remote}/${branch} ${branch}
}
function _gbsetupstream
{
    compadd $(git branch --format '%(refname:short)')
}
compdef _gbsetupstream gbsetupstream

unalias gfandco 2>/dev/null
function gfandco
{
    local remote="$1"
    local rmbranch="${2}"
    if test -z "${rmbranch}"
    then
        rmbranch="${remote}"
        remote=origin
    fi
    local lcbranch="${3:-$rmbranch}"
    if test -n "${lcbranch}"
    then
        git fetch "${remote}" "${rmbranch}"
        git checkout FETCH_HEAD -b "${lcbranch}"
        git branch --set-upstream-to=${remote}/${rmbranch} ${lcbranch} 
    else
        title="$(colorize_bold g)it $(colorize_bold f)etch $(colorize_bold and) $(colorize_bold c)heck$(colorize_bold o)ut"
        cat <<EOM >&2
        $(colorize_bold gfandco) - $title

        $(colorize_bold usage):

            gfandco [remote] remote_branch [local_branch]

            fetches remote_branch from remote (--> FETCH_HEAD)
            checks out FETCH_HEAD into a *new* branch local_branch

            remote defaults to origin
            local_branch defaults to remote_branch but must not exist in any case

        $(colorize_bold examples):

            gfandco origin expiremental

            gfandco upstream dev upstream_dev

        $(colorize_bold "defined in"): $local_file

EOM
    fi

}


# unalias grbih 2>/dev/null
# function grbih
# {
#     local number=$1
#     git rebase -i HEAD~$number
# }

unalias grelease_tag 2>/dev/null
function grelease_tag
{
    local version=$1
    local remote=${2:-origin}
    if test -n "${version}"
    then
        git tag -a "${version}" -m "REL: ${version}"
        git push --tags ${remote}
    else
        cat <<EOM >&2
        usage:
            grelease_tag version [remote]

            creates a new tag named <version> with commit message <version> and pushes the
            tags to remote
            remote defaults to origin
EOM
    fi
}

# unalias grb_with_fetch 2>/dev/null
# function grb_with_fetch {
#     case "$1" in
#         (-h|--help) _grb_with_fetch_help $0;;
#         *) _grb_with_fetch_impl;;
#     esac
# }
# function _grb_with_fetch_help {
#     local branch=$(colorize_blue 'bracnch')
#     local remote=$(colorize_green 'remote')
#     local title=$(colorize_blue_inverse 'Git Rebase with fetched branch')
#     cat - <<EOH

# $title

# Usage:
#     $1 [$branch] [$remote]

#     \`$branch\` defaults to master
#     \`$remote\` defaults to origin


#     the following sequence of commands is executed

#         git fetch \`$remote\`  \`$branch\`
#         git rebase FETCH_HEAD 
# EOH
# }

# function _grb_with_fetch_impl {
#     local branch=${1:-master}
#     local remote=${2:-origin}
#     git fetch $remote $branch
#     git rebase FETCH_HEAD
# }

alias grflog='git reflog --date iso'
# List Remote Git Branches By Author sorted by committerdate
# git for-each-ref --format='%(committerdate) %09 %(authorname) %09 %(refname)' | sort -k5n -k2M -k3n -k4n
