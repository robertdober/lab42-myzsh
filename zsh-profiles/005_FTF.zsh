export ZSH_PROFILES_DIR=~/etc/zsh-profiles

alias tattach='tmux attach-session -t '

alias l='lab42_ls '
# alias ll="ls -ltrh "
unalias ll 2>/dev/null
export __=""
function ll {
    __=$( ls -tr $@ | tail -1)
    ls -ltrh $@
}
alias -- -='cd -'

unalias md 2>/dev/null
function md
{
	mkdir -p $1
	cd $1
}
# Override system settings

unalias encrypt 2>/dev/null
function encrypt
{
    local inp="$1"
    local outp="${2:-${inp}.enc}"
    openssl enc -aes-256-cbc -in "${inp}" -out "${outp}"
}

alias decrypt='openssl enc -aes-256-cbc -d -in '
