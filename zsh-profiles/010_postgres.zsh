
export PostgresDBDir="/usr/local/var/postgres"
alias postgres_start="pg_ctl -D $PostgresDBDir start "
alias postgres_status="pg_ctl -D $PostgresDBDir status "
alias postgres_stop="pg_ctl -D $PostgresDBDir stop "
