alias used_mem_in_kb="ps -o rss= -p "

unalias used_mem_in_mb 2>/dev/null
function used_mem_in_mb {
    m=$(used_mem_in_kb $1)
    echo $(( m / 1000 ))
}
unalias used_mem_for 2>/dev/null
function used_mem_for {
    total=0
    ps -ef | grep "$1" | grep -v grep | while read _x pid _rest; do
        this=$(used_mem_in_mb $pid)
        echo "$pid: ${this}M"
        total=$(( total + this ))
    done
    colorize_blue "total: "
    colorize_green "${total}M"
    echo
}
