export Lab42ProjectDir=$Lab42MyZsh/projects
# function project
# ----------------
function project {
    if test -z "$1"; then
        tmux ls
    else
        project_with_arg $@
    fi
}
function project_with_arg {
    local project_file=${Lab42ProjectDir}/$1
    if test -r ${project_file}; then
        shift
        ${project_file} $@
    else
        tmux attach -t $1
    fi
}
function _project {
    compadd $(ls ${Lab42ProjectDir})
}
alias p='project '
compdef _project project

alias xomodoro="${HOME}/git/elixir/xomodoro/xomodoro "
#
# Named Projects
#
# export Lab419Streams=${Lab419}/streams

