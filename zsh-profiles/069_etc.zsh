function align_date_with_names {
    for file in * ; do
        touch $file
    done
}
local rename_for_unix_mapper='''
$lines = $lines
    .map{|s|%<mv "#{s}" #{s.gsub(%r{[^-\.\s_[[:alnum:]]]}){|x| "%#{x.ord.to_s(16)}"}.gsub(%r{\s}, "__")}>}
'''
function rename_for_unix {
    ruby -e '$lines = $stdin.readlines.map(&:chomp)' \
         -e "${rename_for_unix_mapper}" \
         -e 'puts $lines'
}
# given files a.y, b.y, c.z
# prefix_and_count x => mv a.x x_a_0001.x
# prefix_and_count x => mv c.z x_c_0003.z
function mv_with_prefix_and_count {
    local prefix="$1"
    local width="${2:-4}"
    ruby -e 'lines = $stdin.readlines.map(&:chomp)' \
         -e "prefix=%{$prefix}" \
         -e "width=${width}" \
         -e "c=0" \
         -e 'lines = lines.map{|line|c+=1;a,b=line.split(".");"mv #{line} #{prefix}_#{a}_#{"%0#{width}d" % c}.#{b}"}'
         -e 'puts lines'
}
# mv_with_pattern .png .jpg
# a.png => a.jpg
function mv_with_pattern {
    local trigger="$1"
    local new="$2"
    ruby -e 'lines = $stdin.readlines.map(&:chomp)' \
         -e "trigger = Regexp.compile(Regexp.escape(%{$trigger}))" \
         -e "new = %{$new}" \
         -e 'lines = lines.filter{|s| trigger.match?(s) }' \
         -e 'lines = lines.map{|line| "mv #{line} #{line.sub(trigger, new)}" }' \
         -e 'puts lines'
}
# mv_to_unique
#  orig.ext => <unique_id>.ext
# mv_to_unique --with-count|-c
#  orig.ext => <unique_id>_<count>.ext
# mv_to_unique <prefix>
#  orig.ext => <prefix><unique_id>.ext
# mv_to_unique --with-count|-c prefix
#  orig.ext => <prefix><unique_id>_<count>.ext
# mv_to_unique --with-date|-d prefix
#  orig.ext => <prefix><timestamp>_<count>.ext
function ll_empty {
    find . -maxdepth 1 -size 0  -type f -printf '"%f"\n'
}
function ll_date {
    local filter='.*'
    while test $# -gt 0; do
        case $1 in
            mov*) filter='\.mp4"|\.m4v"'; shift; break;;
            vid*) filter='\.mp4"|\.m4v"'; shift; break;;
            pi*) filter='\.jpe?g"|\.png"'; shift; break;;
            pdf) filter='\.pdf"'; shift; break;;
            *) break;;
        esac
    done
    local day=${1:-$(date +'%d')}
    local mon=${2:-$(date +'%b')}
    find . -maxdepth 1 -type f -printf "%c %f\n" | awk '$2 == "'${mon}'" && $3 == "'${day}'" { print "\"" substr($0,37) "\"" }' | grep -E "${filter}"
}

function randomize {
    if test $# -gt 0
    then
        ruby -e 'puts($stdin.readlines.map(&:chomp).sort_by { rand }.take('$1'))'
    else
        ruby -e 'puts($stdin.readlines.map(&:chomp).sort_by { rand })'
    fi
}

function randomize_q {
    if test $# -gt 0
    then
        ruby -e 'puts($stdin.readlines.map(&:chomp).map(&:inspect).sort_by { rand }.take('$1'))'
    else
        ruby -e 'puts($stdin.readlines.map(&:chomp).map(&:inspect).sort_by { rand })'
    fi
}

function reverse {
    ruby -e 'puts($stdin.readlines.map(&:chomp).reverse)'
}

function ll_zip {
    ls -tr1 | awk '/'$1'/{ x = "\"" $0 "\""; sub("'$1'","'$2'"); print x, " \"", $0, "\""}'
}

