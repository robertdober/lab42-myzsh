
function nip2 {
    local source_file="$TMP/vim.$$.$RANDOM"
    lab42_init_vim $@ > ${source_file}
    nvim -S ${source_file}
    if test -n "$DBG_NIP2"; then
        echo ${source_file}
        return
    fi
    rm ${source_file}
}
# spacemacs
# alias spacemacs='emacs -nw'
function vim_with_colorscheme
{
        local colorscheme="$1"
        shift
        vim -c "colorscheme ${colorscheme}" $@
}

function vim_dark
{
        vim -c "NERDTree ." -c "set background=dark" -c "colorscheme solarized" $@
}


function vim_from_grep
{
    local files=$( git grep -zl "$1" | tr '\0' ' ' )
    eval "vim $files"
}

load_config_from_file config/vim_profiles.txt Lab42VimProfiles


########################################################################
# vip
# Like vip but also openes files
# entries are
#   - directory      -> opened with NERDTree in new tab
#   - directory:file -> opened with NERDTree in new tab & file in right pane
#   - file           -> opened with tabnew
#   - <any of the above>! -> tab to be focused on
########################################################################
function vip_add_source_file_entry {
    local entry="$1"
    local entries=(${(s/:/)entry})
    local file=""
    if test ${#entries} -gt 1
    then
        entry="${entries[1]}"
        file="${entries[2]}"
    fi
    if test -d "${entry}"
    then
        echo tabnew
        echo "sleep 20ms"
        echo "NERDTree ${entry}" 
        vip_open_file_in_right_pane ${entry}/${file}
    else
        echo "tabnew ${entry}"
    fi
}
function vip_focus {
    local count=$1
    local focus=$2
    if test $focus -lt $count
    then
        echo "for i in range($((count - focus - 1))) | exec 'tabprev' | endfor"
    fi
}
function vip_init_source_file {
    cat <<-ENDOFVIM
function! Right() " {{{{{
  exec 'normal l'
endfunction " }}}}}
ENDOFVIM
}
function vip_open_file_in_right_pane {
    if test $# -gt 0
    then
        echo "call Right()"
        echo "edit $1"
    fi
}
function vip2 {
    if test "$1" == "-h"
    then
        _help vip2
    else
        _vip2__impl $@
    fi
}

function _vip2__impl {
    local source_file="$TMP/vim.$$.$RANDOM"
    local profile=$1
    local tabcount=0
    local focuson=0
    local vim=${USE_VIM:-vim}
    shift
    vip_init_source_file > ${source_file}
    for tab
    do
        if expr ${tab} : '.*^'
        then
            tab="${tab%^}"
            focuson=${tabcount}
        fi
        vip_add_source_file_entry "${tab}" >> ${source_file}
        tabcount=$((tabcount+1))
    done
    vip_focus ${tabcount} ${focuson} >> ${source_file}
    if $debug_vip
    then
        cat ${source_file}
    fi
    eval "${vim} ${Lab42VimProfiles[${profile}]} -S ${source_file}"
    rm ${source_file}
}
# for client scripts
# as vip is now the old vip2
alias vip='vip2 '

function nip {
    USE_VIM=$(which nvim) vip2 $@
}

function vi
{
    local dest="${1:-.}"
    if test -d ${dest}
    then
        test $# -gt 0 && shift
        (
            cd ${dest}
            vim_with_colorscheme solarized -c "NERDTree ." $@
        )
    else
        vim_with_colorscheme solarized $@
    fi
}

function vic
{
    local colorscheme="$1"
    shift
    vi $* -c "colorscheme ${colorscheme}"
}


function vi_with_commands
{
    local name="$1"
    shift
    local commands=""
    for command
    do
        commands="$commands -c '$command'"
    done
    
    vi $name $commands
    
}
unalias vimcp 2>/dev/null
function vimcp
{
        vi $@ $(readclipboard)
}

unalias vimatline 2>/dev/null
function vimatline {
  vim -c 'call lab42#files#open_at_line("'$1'")'
}

unalias ivim 2>/dev/null
# Parameters are in that order
# [colorscheme] 
function ivim {
        local x=1
        test $x -le ${#@} && local file=${@[-x]}
        x=$((x+1))
        test $x -le ${#@} && local colors=${@[-x]}
        x=$((x+1))
        test $x -le ${#@} && local backgr=${@[-x]}

        file=${file:-.}
        colors=${colors:-solarized}
        backgr=${backgr:-dark}

        vi -c "colorscheme ${colors}" -c "set background=${backgr}" -c "NERDTree ${file}"
}

alias nvim-gruv="nvim -c 'colorscheme gruvbox|set bg=dark' "
alias ni="nvim -c 'colorscheme gruvbox|set bg=dark' "
