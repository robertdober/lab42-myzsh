if test $(uname) != "Darwin"
then
    return
fi

alias sqlite3='/usr/local/Cellar/sqlite/3.13.0/bin/sqlite3 '
# alias mvim=/usr/local/Cellar/macvim/8.1-156/bin/vim

# function mip {
#     USE_VIM=/usr/local/Cellar/macvim/8.1-156/bin/vim vip2 $@
# }
alias code='/Applications/Visual\ Studio\ Code.app/Contents/Resources/app/bin/code '

export _CurrentNwSv=""
export _CurrentDNSServers=""
function NwSvDetermine {
    local ipaddr=$(NwSvGetIPAddr)
    case ${ipaddr} in
        10*) _CurrentNwSv='AX88179 USB 3.0 to Gigabit Ethernet'
           _CurrentDNSServers='10.20.0.1 10.20.0.2'
            sudo networksetup setdnsservers $_CurrentNwSv 10.20.0.1 10.20.0.2;;
        *) _CurrentNwSv='Wi-Fi'
           _CurrentDNSServers='192.168.1.1'
            sudo networksetup setdnsservers $_CurrentNwSv 192.168.1.1;;
    esac
    echo "current => $_CurrentNwSv DNS: $_CurrentDNSServers"

}
##NwSvDetermine

function NwSvGetIPAddr {
     ifconfig | awk '/inet /{ print $2}' | grep -v '127.0.0.1'
}

alias NwCurrentLocation='networksetup -getcurrentlocation '

function NwSvList {
    networksetup -listallnetworkservices
    echo "current => ${_CurrentNwSv}"
}
function NwSvUse {
    _CurrentNwSv="$*"
}

alias NwSvDisable='NwSvCommand setnetworkserviceenabled off'
alias NwSvEnable='NwSvCommand setnetworkserviceenabled on'
function NwSvInfo {
    echo "$_CurrentNwSv"
    NwSvCommand getnetworkserviceenabled
}

function NwSvCommand {
    local command="$1"
    shift
    set -x
    sudo networksetup -${command} "${_CurrentNwSv}" $*
    set +x
}


alias NwSvGetDnsServers='NwSvCommand getdnsservers '
function NwSvSetDnsServers {
    sudo networksetup setdnsservers $_CurrentNwSv $*
}

##
## Applications
##
alias libre-office="open /Applications/LibreOffice.app "

export Lab42BrewBin=/usr/local/bin
