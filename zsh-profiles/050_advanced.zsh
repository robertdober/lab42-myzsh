# function swap-files
# {
#     set -x
#    local tmpfile=${TMP:-~/tmp}/$(basename $1).$$
#    cp -ap $1 $tmpfile
#    cp -ap $2 $1
#    cp -ap $tmpfile $2
#    rm tmpfile
#    set +x
# }
function _normalized_filetype
{
        file "$1" | awk -F : -e '{gsub("^ *","",$2);print $2}'
}
#
#  Context dependent open
#
#  Will detect from $(file) or extension of first param what to do
function o
{
        # local ftype="$( file "$1" | sed -Ee 's/.*?:\s+//' )"
        local ftype="$(_normalized_filetype "$1")"
        case $ftype in  
                'PDF document,'*) nohup xreader $1 &;;
                'Composite Document File'*) nohup libreoffice $1 &;;
                'OpenDocument'*) nohup libreoffice $1 &;;
                'exported SGML document'*) vim -p $@;;
                'JPEG image data'*) nohup pix $@;;
                'GIF image data'*) nohup pix $@;;
                'XML document text'*) vim -p $@;;
                'UTF-8 Unicode text'*) vim -p $@;;
                'Zip archive data'*) nohup libreoffice $1 &;;
                'Microsoft'*) nohup libreoffice $1 &;;
                directory*) vim $1;;
        *) echo "$ftype unknown";;
        esac
                
}

function rename_by_date
{
  ls "$@" | ruby ${Lab42MyZsh}/ruby/rename.rb
}

function repeat_cmd
{
    local interval="${1}"
    shift
    local command="$@"
    while :; do
        echo "$(date +%T)"
        echo "$(eval $command)"
        sleep ${interval}
    done
}
