#compdef mix
#autoload
function _mix
{
        compadd $(mix --help | awk '{print $2}')
}
_mix
