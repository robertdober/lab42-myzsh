#!/usr/local/bin/zsh

function cond_cat
{
    if test -r "$1"; then
        cat "$1"
    fi
}
