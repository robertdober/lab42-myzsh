function set_tmux_status_colours {
    tmux set status-style bg=colour$1,fg=colour$2
} 
function set_tmux_current_colours {
    tmux set -g window-status-current-style bg=colour$1,fg=colour$2
}
function set_tmux_left_colours {
    tmux set status-left-style bg=colour$1,fg=colour$2
}
function set_tmux_all_colours {
	# $1: status_bg
	# $2: status_fg
	# $3: current_bg
	# $4: current_fg
	# $5: left_bg
	# $6: left_fg
	set_tmux_status_colours $1 $2	
	set_tmux_current_colours $3 $4
	set_tmux_left_colours $5 $6
}
