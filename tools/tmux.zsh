# # export window_rc_file=${window_rc_file:-.tmux.window}
# # export session_rc_file=${session_rc_file:-.tmux.session}

# export local_session_init_file=${local_session_init_file:-${session_home_dir}/.local.init-tmux-session.zsh}
# export local_window_init_file=${local_window_init_file:-${session_home_dir}/.local.init-tmux-window.zsh}

# function attach_or_create {
#     tmux attach -t $session_name 2>/dev/null || create_new_session
# }

# function _get_before_init_session_code_ {
#     local file=$1
#     which -s before_init >/dev/null || return
#     which before_init > $file
#     ex -u /dev/null -c '1d|$d|wq' $file
# }
# function _before_init_session_ {
#     touch $local_session_init_file
#     local tmpfile=$(tempfile)
#     _get_before_init_session_code_ ${tmpfile}
#     echo "source ${Lab42MyZsh}/tools/tmux_runtime.zsh" > ${local_session_init_file}
#     echo "export project_home=$session_home_dir" >> $local_session_init_file
#     cat $tmpfile >> ${local_session_init_file}
#     rm $tmpfile

#     # N.B. ** Do not move this code into a function **
#     caller=$(echo ${funcfiletrace[$#funcfiletrace]} | awk '{sub(":[0-9]+$", ""); print}')
#     local_candidate="$(dirname $caller)/.$(basename $caller).local.zsh"
#     if test -r ${local_candidate}; then
# 	echo "source ${local_candidate}" >> ${local_session_init_file}
#     fi
# }
# function _get_before_init_window_code_ {
#     local file=$1
#     which -s before_init_window >/dev/null || return
#     which before_init_window > $file
#     ex -u /dev/null -c '1d|$d|wq' $file
# }
# function _before_init_window_ {
#     touch $local_window_init_file
#     local tmpfile=$(tempfile)
#     _get_before_init_window_code_ ${tmpfile}
#     echo "source ${Lab42MyZsh}/tools/tmux_runtime.zsh" > ${local_window_init_file}
#     echo "export project_home=$session_home_dir" >> $local_window_init_file
#     cat $tmpfile >> ${local_window_init_file}
#     rm $tmpfile

#     # N.B. ** Do not move this code into a function **
#     caller=$(echo ${funcfiletrace[$#funcfiletrace]} | awk '{sub(":[0-9]+$", ""); print}')
#     local_candidate="$(dirname $caller)/.$(basename $caller).local.zsh"
#     if test -r ${local_candidate}; then
# 	echo "source ${local_candidate}" >> ${local_window_init_file}
#     fi
# }
# function create_new_session {
#     cd $session_home_dir
#     set -x
#     tmux new-session -s $session_name -d
#     _before_init_session_
#     _before_init_window_
#     init_local_session
#     init_local_window
#     set +x

#     init_new_session
#     tmux attach -t $session_name
# }

# function init_local_session {
#     test -r "${local_session_init_file}" || return
#     tmux send-keys "source ${local_session_init_file}" C-m
# }

# function init_local_window {
#     test -r "${local_window_init_file}" || return
#     tmux send-keys "source ${local_window_init_file}" C-m
# }


# function export_env_var {
#     send_keys "export $1='$2'"
# } 

# function send_keys {
#     tmux send-keys $@ C-m
# }

# function new_win {
#     tmux new-window -n "$1"
# #    init_local_window
#     while : ; do
#         if test $# -le 1; then
#             break
#         fi
#         shift
#         tmux send-keys "$1"
#         tmux send-keys C-m
#     done
# }
# # A new_win with a custom init window file as $1
# function new_window {
#     tmux new-window -n "$1"
# #    init_local_window
#     shift
#     for keys
#     do
#         tmux send-keys "$keys" C-m
#     done 
# }
# # this is new_window without sending the C-m at the end of the sent keys
# function new_window1 {
#     tmux new-window -n "$1"
# #    init_local_window
#     shift
#     for keys
#     do
#         tmux send-keys "$keys"
#     done 
# }


# function select_window
# {
#         tmux select-window -t :$1
# }

# # Opens a window with a new command that is defined in the tmux session

# function wait_for_window {
#     tmux wait-for window_finished_$session_name
# }

