
local_session_init=${local_session_init:-${session_home_dir}/${local_session_file}}
function attach_or_create {
    tmux attach -t $session_name 2>/dev/null || create_new_session
}

function create_new_session {
    set -x
    tmux new-session -s $session_name -d -c $session_home_dir
    _source_local_file

    init_session
    set +x
    sleep 2
    tmux attach -t $session_name
}


function new_window {
    tmux new-window -t $session_name -n $1 -c $session_home_dir
    _source_local_file
}


function prepare_keys {
    tmux send-keys -t $session_name $@
}

function send_keys {
    tmux send-keys -t $session_name $@ C-m
}


#-------------------------------------------
#
#  Private
#
#-------------------------------------------

function _source_local_file {

    test -r $local_session_init && tmux send-keys -t $session_name "source $local_session_init" C-m

    which local_window_init 2>/dev/null >&2 && local_window_init
}
