use Mix.Config
config :xmux, sys_interface: Xmux.SysInterface.Implementation
config :xmux, themes_dir: Path.join(
    ~w[#{System.get_env("Lab42MyZsh")} tmux themes])
