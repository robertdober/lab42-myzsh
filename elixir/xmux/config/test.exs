use Mix.Config
config :xmux, themes_dir: Path.expand(
  Path.join(
    ~w[test fixtures themes]))
config :xmux, sys_interface: Xmux.SysInterface.Mock
