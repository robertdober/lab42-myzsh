defmodule Xmux do

  use Xmux.Types
  alias Xmux.Options
  alias Xmux.Commands

  @moduledoc """
  The escript main module
  """

  @sys_interface Application.fetch_env!(:xmux, :sys_interface)

  @spec main(OptionParser.argv()) :: cmd_return
  def main(args) do
    case args
      |> Options.parse
      |> Options.validate(number: "> 0") do
        {:ok, options} -> create_and_run(options)
        {:error, error} -> IO.puts(:stderr, error); raise "Illegal Syntax"
    end
  end


  @spec create_and_run( Options.t ) :: cmd_return
  defp create_and_run(options) do
    options
    |> Commands.create
    |> @sys_interface.cmds
  end

end
