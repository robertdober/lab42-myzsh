defmodule Xmux.Config do
  
  @moduledoc """
  Access to this projects configuration from `config/config.exs`
  """

  @doc """
  Where are themes?
  """
  @spec  themes_dir() :: String.t
  def themes_dir do 
    Application.fetch_env!(:xmux, :themes_dir)
  end
end
