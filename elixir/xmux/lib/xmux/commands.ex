defmodule Xmux.Commands do

  use Xmux.Types

  alias Xmux.Options
  
  @moduledoc """
  Create the commands to create a tmux session as specified by the
  options
  """

  @doc """
  Take options, create commands
  """

  @spec create( Options.t ) :: cmd_returns
  def create(options)
  def create({_, positionals}) do
    positionals
  end
end
