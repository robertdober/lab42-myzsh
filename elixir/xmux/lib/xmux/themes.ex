defmodule Xmux.Themes do

  alias Xmux.Config

  @moduledoc """
  Access themes resource
  """

  @doc """
  get yaml for one theme
  """
  @spec get( atom ) :: %{}
  def get(theme \\ :default) do
    Path.join(Config.themes_dir, "#{theme}.yml")
    |> YamlElixir.read_from_file!
  end
end
