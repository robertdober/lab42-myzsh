defmodule Xmux.Options do
  use Xmux.Types

  @typep result      :: either(t)
  @typep errors      :: list(String.t)
  @typep positionals :: list(String.t)
  @typep keywords    :: %{atom => String.t}

  @type t :: {keywords, positionals}

  @doc """
  parse arg list

      iex(0)> Options.parse(~w[a: 1 hello b: 2])
      {%{a: "1", b: "2"}, ~w[hello]}
  """
  @spec parse( list(String.t) ) :: t
  def parse(args) do
    _parse(args, %{}, [])
  end

  @doc """
  validate parsed options

       iex(1)> {%{a: "1"}, ~w[hello world]}
       ...(1)> |> Options.validate(needed: ~w[a]a, number: "> 1")
       {:ok, {%{a: "1"}, ~w[hello world]}}
  """
  @spec validate( t, Keyword.t ) :: result
  def validate({kwds, positionals}=options, spec \\ []) do
    needed = Keyword.get(spec, :needed, [])
    number = Keyword.get(spec, :number, "> -1")
    number_checker = _make_number_checker(number)
    errors = _needed_check(kwds, needed, [])
    errors1 = _number_check(positionals, number_checker, number, errors)
    if Enum.empty?(errors1) do
      {:ok, options}
    else
      {:error, errors1|>Enum.join("\n")}
    end
  end

  @spec _make_number_checker( String.t ) :: ((non_neg_integer) -> boolean)
  defp _make_number_checker(number_spec)
  defp _make_number_checker(">" <> rest) do
    {compare, _} =rest |> String.trim |> Integer.parse
    fn x ->
      x > compare
    end
  end
  defp _make_number_checker("<" <> rest) do
    {compare, _} =rest |> String.trim |> Integer.parse
    fn x ->
      x < compare
    end
  end
  defp _make_number_checker("=" <> rest) do
    {compare, _} =rest |> String.trim |> Integer.parse
    fn x ->
      x == compare
    end
  end
  defp _make_number_checker(spec) do
    raise "This is not a number spec #{inspect spec}\n\tLegal Syntax: [>|<|=] \s* number"
  end

  @spec _needed_check( keywords, list(atom), errors) :: errors
  defp _needed_check(kwds, needed, errors)
  defp _needed_check(_kwds, [], errors), do: errors
  defp _needed_check(kwds, [needed|rest], errors) do
    errors1 =
      if Map.has_key?(kwds, needed) do
        errors
      else
        ["Missing key #{inspect needed} in keywords #{inspect kwds}" | errors]
      end
    _needed_check(kwds, rest, errors1)
  end

  @spec _number_check( positionals, (non_neg_integer -> boolean), String.t, errors) :: errors
  defp _number_check(positionals, checker, spec, errors) do
    if checker.(Enum.count(positionals)) do
      errors
    else
      ["Positional args: #{inspect positionals} do not conform to count #{inspect spec}" | errors]
    end
  end

  @kwds ~r{(.*):\z}

  @spec _parse( positionals, keywords, positionals) :: t
  defp _parse(args, kwds, rest)
  defp _parse([], kwds, rest), do: {kwds, Enum.reverse(rest)}
  defp _parse([arg|args], kwds, rest) do
    case Regex.run(@kwds, arg) do
      [_, kwd] -> _parse_kwd(args, String.to_atom(kwd), kwds, rest)
      _        -> _parse(args, kwds, [arg|rest])
    end
  end

  @spec _parse_kwd( positionals, atom, keywords, positionals ) :: t
  defp _parse_kwd([], kwd, _, _), do: raise "Missing value for #{kwd}"
  defp _parse_kwd([arg|args], kwd, kwds, rest) do
    _parse(args, Map.put(kwds, kwd, arg), rest)
  end

end
