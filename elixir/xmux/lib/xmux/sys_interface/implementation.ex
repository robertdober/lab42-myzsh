defmodule Xmux.SysInterface.Implementation do

  use Xmux.Types

  @behaviour Xmux.SysInterface.Behavior

  @spec sleep( number ) :: :ok
  def sleep time do
    Process.sleep time
  end

  @spec cmd( String.t, list(String.t) ) :: cmd_return 
  def cmd command, args do
    System.cmd(command, args)
  end

  @spec cmds( list({String.t, list(String.t)}) ) :: cmd_return
  def cmds commands_and_args do
    commands_and_args
    |> Enum.reduce({"", 0}, &_cmd/2)
  end


  @spec _cmd({String.t, list(String.t)}, cmd_return) :: cmd_return
  defp _cmd(cmd_and_args, status)
  defp _cmd({cmd, args}, {_, 0}) do
    case System.cmd(cmd, args) do
      {out, 0} -> IO.write(out); {out, 0}
      {err, n} -> IO.write(:stderr, err); {err, n}
    end
  end
  defp _cmd({cmd, args}, x) do
    IO.puts(:stderr, "skipping cmd #{inspect {cmd, args}}")
    x
  end
end
