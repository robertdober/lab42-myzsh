defmodule Xmux.SysInterface.Behavior do
  use Xmux.Types

  @callback sleep(number()) :: :ok

  @callback cmd(String.t, list(String.t)) :: cmd_return
  @callback cmds(cmd_returns) :: cmd_return
end
