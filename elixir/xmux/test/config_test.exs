defmodule ConfigTest do
  use ExUnit.Case
  
  alias Xmux.Config


  describe "themes_dir" do
    test "set for test env" do
      assert Config.themes_dir == Path.expand(Path.join(~w[test fixtures themes]))
    end
  end
end
