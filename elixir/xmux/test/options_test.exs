defmodule OptionsTest do
  use ExUnit.Case

  
  alias Xmux.Options
  doctest Options

  describe "parsing" do
    test "empty" do
      assert Options.parse([]) == {%{}, []}
    end
    test "only positionals" do
      args = ~w{a b c}
      assert Options.parse(args) == {%{}, args}
    end
    test "only keywords" do
      args = ~w{a: b c: d}
      assert Options.parse(args) == {%{a: "b", c: "d"}, []}
    end
    test "mixed" do
      args = ~w{hello a: b world c: d etc}
      assert Options.parse(args) == {%{a: "b", c: "d"}, ~w{hello world etc}}
    end
  end

  describe "validation" do
    @options {%{
      a: "42",
      b: "43"
    }, ~w[alpha beta gamma]}

    test "all ok" do
      assert Options.validate(@options, needed: ~w{a b}a, number: "> 2") == {:ok, @options} 
    end

    test "missing" do
      assert Options.validate(@options, needed: ~w{a b c}a, number: "> 2") ==
        {:error, "Missing key :c in keywords %{a: \"42\", b: \"43\"}"} 
    end
    test "too few args and missing" do 
      assert Options.validate(@options, needed: ~w{a b c}a, number: "> 3") ==
        {:error,
         "Positional args: [\"alpha\", \"beta\", \"gamma\"] do not conform to count \"> 3\"\nMissing key :c in keywords %{a: \"42\", b: \"43\"}"
       }
    end
  end

end
