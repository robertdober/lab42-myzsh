defmodule ThemesTest do
  use ExUnit.Case
  
  alias Xmux.Themes

  describe "access to default" do
    @expected %{
	"status" => %{"current_style" => "fg=blue,bg=green", "style" => "bg=white,fg=black"},
        "status_left" => %{"length" => 40, "text" => "#[bg=#443333,fg=blue] Session: #S "}}

    test "explicit" do
      assert Themes.get(:default) == @expected
    end

    test "implicit" do
      assert Themes.get() == @expected
    end
	
  end
end
