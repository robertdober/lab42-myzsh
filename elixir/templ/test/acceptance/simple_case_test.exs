defmodule Acceptance.SimpleCaseTest do
  use ExUnit.Case

  describe "a simple case to assure basic functionality, using fixtures/simple.eex and .env" do

    @expected """
     I have a name with a = is not a problem  
     and a 1.42 which can be repeated 1.42, of course!
    """
    test "with the simple fixtures" do

      assert @expected ==
        Templ.Resolver.resolve([ "test/fixtures/simple.eex", "test/fixtures/simple.env" ]) 

    end

    @expected1 """
     I have a Robert 
     and a undefined which can be repeated undefined, of course!
    """
    test "with many envs" do
      assert @expected1 ==
        Templ.Resolver.resolve(["test/fixtures/simple.eex", "test/fixtures/defaults.env",  "test/fixtures/missing.env"])
    end
  end
  
end
