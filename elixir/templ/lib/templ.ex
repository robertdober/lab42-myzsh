defmodule Templ do
  @moduledoc """
  An escript taking an env file with variable declarations like:

      name = Hello World
      version = 1.42.0


  and a template `.eex` file like:

      I am <%= name %> of <%= version %>

  Output to stdout would be

      I am Hello World of 1.42.0


  The value of a variable is all after `= `, that is the first `=` of the line of course.
  """


  @doc """
  The entry point of the escript
  """
  def main(argv) do
    argv
    |> parse_args
    |> process
  end


  defp parse_args(argv) do
    switches = [
      help: :boolean,
      version: :boolean
      ]
    aliases = [
      h: :help,
      v: :version
    ]
    parse = OptionParser.parse(argv, switches: switches, aliases: aliases)
    case  parse  do
      { [ {switch, true } ],  _, _ } -> switch
      { _, [_, _|_] = files,  _ }    -> files
      _                              -> :help
    end
  end


  @help """
  templ <eex_template> <env_files>...
  """
  defp process(:help) do
    IO.puts(:stderr, @help)
  end
  defp process(:version) do
    with {:ok, version} = :application.get_key(:templ, :vsn), do: 
    IO.puts(:stderr, version)
  end
  defp process(files) do
    files |> Templ.Resolver.resolve |> IO.write
  end
end
