defmodule Templ.Resolver do

  @doc """
  Implementing the BL of `Templ.main`
  """
  def resolve([eex_file|env_files]) do
    env_files
    |> extract_variables_from_files()
    |> interpolate(eex_file)
  end


  defp extract_variables_from_files(env_files) do
    env_files
    |> Enum.reduce(%{}, &extract_variables/2)
  end

  defp extract_variables(env_file, map) do
    Map.merge(
      map,
      env_file
      |> File.stream!
      |> read_variables())
  end

  defp interpolate(variables, eex_file) do
    EEx.eval_file(eex_file, Enum.into(variables, []))
  end

  @comment ~r{\A\s*#}
  defp read_variables(file_stream) do
    file_stream
    |> Enum.reject(&Regex.match?(@comment, &1))
    |> Enum.reduce(%{}, &variable_into_map/2)
  end

  @splitter ~r{\s*=\s?}
  defp variable_into_map(line, map) do
    [name, value] =
      line
      |> String.trim_trailing("\n")
      |> String.split(@splitter, parts: 2)
    Map.put(map, String.to_atom(name), value)
  end
end
